require('./bootstrap');

window.Vue = require('vue');

import MainApp from './components/MainApp.vue'


import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'animate.css';


Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)




import VueTypedJs from 'vue-typed-js'
Vue.use(VueTypedJs)

export const eventBus = new Vue();

const app = new Vue({
    el: '#app',
    components: {
        'main-app': MainApp,
    }
});
export default app;
