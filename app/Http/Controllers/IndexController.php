<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function formSubmit(Request $request)
    {

        if ($request->input('lang') === 'en') {
            $messsages = [
                'name.required' => 'Enter your name.',
                'name.string' => 'Enter your name correctly.',
                'email.required' => 'Enter your email.',
                'email.email' => 'Enter your email correctly.',
                'message.required' => 'Enter your message.'
            ];
        } else {
            $messsages = [
                'name.required' => 'نام خود را وارد کنید.',
                'name.string' => 'نام خود را به درستی وارد کنید.',
                'email.required' => 'ایمیل خود را وارد کنید.',
                'email.email' => 'ایمیل خود را به درستی وارد کنید.',
                'message.required' => 'پیام خود را وارد کنید.'
            ];
        }


        $validators = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required',
        ], $messsages);

        if ($validators->fails()) {
            $error = $validators->errors();
            return response()->json(['errors' => $error], 422);
        }

        Contact::create($request->all());
        return response()->json([
            'status' => true
        ], 200);
    }
}
